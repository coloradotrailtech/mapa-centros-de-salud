import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { BLOQUEAR_INTERFACE_MAPA, SELECT_EFECTOR } from '../../redux/actions/general';
import { BLOQUEAR_INTERFACE_BUSQUEDA, FILTRAR_EFECTORES, SORT_EFECTORES, UPDATE_SNACK_ORDEN, LIMPIAR_EFECTORES_FILTRADOS, UPDATE_SNACK_ORDENANDO } from '../../redux/actions/busqueda';
import { View, StyleSheet, FlatList } from 'react-native';
import { List, ToggleButton, Snackbar } from 'react-native-paper';
import IndicadorCarga from '../../components/IndicadorCarga';
import MensajeListado from '../../components/MensajeListado';

class EfectoresFiltrados extends Component {

    static navigationOptions = {
        headerTitle: "Resultados",
        headerStyle: {
            backgroundColor: '#fff',
        },
    };

    componentDidMount() {
        const {
            cartillas,
            coordenadas_en_rango,
            dato_a_buscar,
            efectores,
            ubicacionUsuario,
            BLOQUEAR_INTERFACE_BUSQUEDA,
            FILTRAR_EFECTORES } = this.props;

        if (dato_a_buscar !== {}) {
            BLOQUEAR_INTERFACE_BUSQUEDA(true);
            FILTRAR_EFECTORES({
                cartillas: cartillas,
                dato_a_buscar: dato_a_buscar,
                efectores: efectores,
                ubicacionUsuario: ubicacionUsuario,
                coordenadas_en_rango: coordenadas_en_rango
            });
        }
    }

    render() {

        const {
            cartillas,
            dato_a_buscar,
            efectores,
            refreshing,
            ubicacionUsuario,
            coordenadas_en_rango,
            efectores_filtrados,
            bloquear_interface_busqueda,
            navigation,
            mostrar_snack_orden,
            mostrar_snack_ordenando,
            tipo_ordenamiento,
            BLOQUEAR_INTERFACE_MAPA,
            SELECT_EFECTOR,
            SORT_EFECTORES,
            FILTRAR_EFECTORES,
            UPDATE_SNACK_ORDEN,
            UPDATE_SNACK_ORDENANDO,
        } = this.props;

        const visualizar_detalle = (efector) => {
            BLOQUEAR_INTERFACE_MAPA(true);
            SELECT_EFECTOR(efector);
            navigation.navigate('Detalle');

        }

        function Item({ efector }) {
            return (
                <List.Item
                    title={efector.nombre}
                    description={coordenadas_en_rango ? efector.direccion + "\nA " + efector.distancia + " km de usted" : efector.direccion}
                    left={() => <List.Icon icon="hospital-building" />}
                    onPress={() => visualizar_detalle(efector)}
                />
            );
        }

        function ordenar(orden) {
            SORT_EFECTORES(orden);
            FILTRAR_EFECTORES({
                cartillas: cartillas,
                dato_a_buscar: dato_a_buscar,
                efectores: efectores,
                ubicacionUsuario: ubicacionUsuario,
                coordenadas_en_rango: coordenadas_en_rango
            });

        }

        return (
            <View style={styles.container}>
                {bloquear_interface_busqueda ?
                    <IndicadorCarga />
                    :
                    <View style={styles.container}>
                        {efectores_filtrados.length > 0 ?
                            <List.Section style={styles.container}>
                                <View style={styles.fixToText}>
                                    <List.Subheader>Centros de Salud encontrados: </List.Subheader>
                                    {
                                        coordenadas_en_rango ?
                                            <ToggleButton.Row
                                                style={styles.toogle}
                                                onValueChange={value => ordenar(value)}
                                            >
                                                <ToggleButton icon="sort" value="nombre" />
                                                <ToggleButton icon="directions" value="distancia" />
                                            </ToggleButton.Row>
                                            : null
                                    }
                                </View>
                                <FlatList
                                    data={efectores_filtrados}
                                    renderItem={({ item }) => <Item efector={item} />}
                                    keyExtractor={item => item.cuie}
                                />
                                <Snackbar
                                    visible={mostrar_snack_orden}
                                    onDismiss={() => UPDATE_SNACK_ORDEN(false)}
                                    action={{
                                        label: 'Cerrar',
                                        onPress: () => {
                                            UPDATE_SNACK_ORDEN(false);
                                        },
                                    }}
                                >
                                    {tipo_ordenamiento == "nombre" ? "Ordenado alfabéticamente" : "Ordenado por cercanía"}

                                </Snackbar>
                            </List.Section>
                            :
                            <MensajeListado mensaje={"No existen Centros de Salud con los criterios solicitados"} />
                        }
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    toogle: {
        marginRight: 10,
    }
});

const mapStateToProps = (state) => ({
    cartillas: state.general.cartillas,
    coordenadas_en_rango: state.general.coordenadas_en_rango,
    efectores: state.general.efectores,
    bloquear_interface_busqueda: state.busqueda.bloquear_interface_busqueda,
    dato_a_buscar: state.busqueda.dato_a_buscar,
    efectores_filtrados: state.busqueda.efectores_filtrados,
    ubicacionUsuario: state.general.ubicacionUsuario,
    tipo_ordenamiento: state.busqueda.tipo_ordenamiento,
    mostrar_snack_orden: state.busqueda.mostrar_snack_orden,
    refreshing: state.busqueda.refreshing,
});

const mapDispatchToProps = dispatch => bindActionCreators(
    { BLOQUEAR_INTERFACE_BUSQUEDA, BLOQUEAR_INTERFACE_MAPA, FILTRAR_EFECTORES, SELECT_EFECTOR, SORT_EFECTORES, UPDATE_SNACK_ORDEN, LIMPIAR_EFECTORES_FILTRADOS, UPDATE_SNACK_ORDENANDO }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EfectoresFiltrados);