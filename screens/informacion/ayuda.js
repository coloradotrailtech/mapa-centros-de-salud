import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from 'react-native';
import { Card } from 'react-native-paper';
import Carousel from "react-native-carousel-control";

export default class InformacionAyudaScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>

        <Carousel >
          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 253
              }}
                source={require('../../assets/images/ayuda/informacion/1.png')} />
              <Text style={styles.leyenda}>
                En esta sección encontrarás información de la app, páginas web de interés y teléfonos de interés.
                </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 276
              }}
                source={require('../../assets/images/ayuda/informacion/2.png')} />
              <Text style={styles.leyenda}>
                Si tocas en algún ítem de esta lista de páginas web de interés podrás decidir si visitar esa web a la que hace referencia.
              </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 168
              }} source={require('../../assets/images/ayuda/informacion/3.png')} />
              <Text style={styles.leyenda}>
                Si tocas en algún ítem de esta lista teléfonos de interés podrás decidir si dicar el número al que hace referencia.
              </Text>
            </Card.Content>
          </Card>

        </Carousel>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#666464'
  },
  cabecera: {
    marginTop: 35,
  },
  carta: {
    marginBottom: 8,
  },
  container_header: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagen: {
    width: 256,
    height: 256
  },
  leyenda: {
    marginTop: 10,
    textAlign: 'center',
    color: '#666464'
  },

});
