import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';

export default function MensajeListado({abrir_ayuda, mensaje }) {
    return (
        <View style={styles.container}>
            <Text>{mensaje}</Text>
            <Button
                theme={{ colors: { primary: "#424242" } }}
                icon="help-circle-outline"
                onPress={() => abrir_ayuda()}
                mode="text"
                style={styles.button}>                
                ayuda
            </Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        color: "#fff",
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 10,
        color: '#f16435',
    }
});