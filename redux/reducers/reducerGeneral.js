import { handleActions } from 'redux-actions';

import {
    BLOQUEAR_INTERFACE_MAPA,
    PUSH_CARTILLAS,
    PUSH_EFECTORES,
    SELECT_EFECTOR,
    UPDATE_SNACK_COORDENADAS,
    UPDATE_UBICACION_USUARIO,
    UPDATE_SNACK_ERROR_GENERAL
} from '../actions/general';

import {
    LATITUDE_DELTA,
    LONGITUDE_DELTA,
    LATITUDE_X_DEFECTO,
    LONGITUDE_X_DEFECTO
} from '../../constants/GeoData';

const initialState = {
    bloquear_interface_mapa: false,
    cartillas: [],
    coordenadas_en_rango: false,
    mostrar_snack_coordenadas: true,
    mostrar_snack_error_general: false,
    efector: {},
    efectores: [],
    ubicacionUsuario: "",
}

export default reducerGeneral = handleActions({
    [BLOQUEAR_INTERFACE_MAPA]: (state, action) => {
        return {
            ...state,
            bloquear_interface_mapa: action.payload
        };
    },
    [PUSH_CARTILLAS]: (state, action) => {
        return {
            ...state,
            cartillas: action.payload
        };
    },
    [PUSH_EFECTORES]: (state, action) => {
        return {
            ...state,
            efectores: action.payload
        };
    },
    [SELECT_EFECTOR]: (state, action) => {
        return {
            ...state,
            efector: {
                ...action.payload,
                cartillas: state.cartillas.filter(cartilla => cartilla.cuie == action.payload.cuie)
            },
            bloquear_interface_mapa: false
        };
    },
    [UPDATE_SNACK_COORDENADAS]: (state, action) => {
        return {
            ...state,
            mostrar_snack_coordenadas: action.payload
        };
    },
    [UPDATE_SNACK_ERROR_GENERAL]: (state, action) => {
        return {
            ...state,
            mostrar_snack_error_general: action.payload
        };
    },
    [UPDATE_UBICACION_USUARIO]: (state, action) => {
        return {
            ...state,
            coordenadas_en_rango: action.payload.coordenadas_en_rango,
            mostrar_snack_coordenadas: !action.payload.coordenadas_en_rango,
            ubicacionUsuario: {
                latitude: action.payload.coordenadas_en_rango ? action.payload.coordenadas.latitude : LATITUDE_X_DEFECTO,
                longitude: action.payload.coordenadas_en_rango ? action.payload.coordenadas.longitude : LONGITUDE_X_DEFECTO,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            }
        };
    },
}, initialState);