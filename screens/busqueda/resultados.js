import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { List, Button } from 'react-native-paper';
import MensajeListado from '../../components/MensajeListado';
import { Ionicons } from '@expo/vector-icons';

export default function Resultados({ abrir_ayuda, resultados_filtrados, filtrar_efectores }) {
    return (
        <View style={styles.container}>
            {resultados_filtrados.length > 0 ?
                <ScrollView >
                    <List.Section>
                        <View style={styles.fixToText}>
                            <List.Subheader>Coincidencias encontradas:</List.Subheader>
                            <Button
                                theme={{ colors: { primary: "#424242" } }}
                                onPress={() => abrir_ayuda()}
                                mode="text"
                                style={styles.button}>
                                <Ionicons
                                    name="md-help-circle-outline"
                                    size={20}
                                    style={styles.searchIcon}
                                />
                            </Button>
                        </View>
                        {
                            resultados_filtrados.map((resultado, key) =>
                                (
                                    <List.Item
                                        key={key}
                                        title={resultado.dato.nombre}
                                        onPress={() => filtrar_efectores(resultado)}
                                        left={() =>
                                            resultado.tipo === "efectores" ?
                                                <List.Icon icon="hospital-building" /> :
                                                resultado.tipo === "localidades" ?
                                                    <List.Icon icon="city-variant-outline" />
                                                    :
                                                    <List.Icon icon="hospital" />
                                        }
                                    />
                                ))
                        }
                    </List.Section>
                </ScrollView>
                :
                <MensajeListado abrir_ayuda={abrir_ayuda} mensaje={"No se encontraron coincidencias"} />
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});