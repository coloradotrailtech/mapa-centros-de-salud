import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FILTRAR_RESULTADOS, LIMPIAR_RESULTADOS, UPDATE_DATO_ESCRITO } from '../../redux/actions/busqueda';
import { StyleSheet, View, TextInput } from 'react-native';
import { Button } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';


class InputBusqueda extends Component {

  render() {

    const { efectores,
      localidades,
      servicios,
      dato_escrito,
      FILTRAR_RESULTADOS,
      LIMPIAR_RESULTADOS,
      UPDATE_DATO_ESCRITO } = this.props;

    const filtrar_resultados = (dato) => {
      if (dato.length > 3) {
        FILTRAR_RESULTADOS({
          dato: dato,
          localidades: localidades,
          servicios: servicios,
          efectores: efectores
        });
      } else if (dato.length == 0) {
        LIMPIAR_RESULTADOS();
      } else {
        UPDATE_DATO_ESCRITO(dato);
      }
    }

    const abrir_ayuda = () => {
      navigation.navigate('MapaAyuda');
  }

    return (
      <View style={styles.searchSection}>
        <Ionicons
          name="md-search"
          size={20}
          style={styles.searchIcon}
        />
        <TextInput
          style={styles.input}
          placeholder="Buscar por nombre o localidad o servicio"
          onChangeText={valor => filtrar_resultados(valor)}
          underlineColorAndroid="transparent"
          defaultValue={dato_escrito}
        />
        {
          dato_escrito.length > 0 ?
            <Button theme={{ colors: { primary: "#424242" } }}
              onPress={() => LIMPIAR_RESULTADOS()}
              mode="text"
              style={[styles.searchIcon, styles.deleteIcon]}>
              <Ionicons
                name="md-close"
                size={20}
                style={styles.searchIcon}
              />
            </Button>
            :
            null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchSection: {
    marginLeft: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  deleteIcon: {
    marginRight: 0,
    marginLeft: 0,
  },
  input: {
    flex: 1,
    paddingTop: 5,
    paddingRight: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    backgroundColor: '#f0f0f0',
    color: '#424242',
  },
  searchIcon: {
    padding: 3,
    marginRight: 5,
},
});

const mapDispatchToProps = dispatch => bindActionCreators({
  FILTRAR_RESULTADOS,
  LIMPIAR_RESULTADOS,
  UPDATE_DATO_ESCRITO
}, dispatch);

const mapStateToProps = (state) => ({
  dato_escrito: state.busqueda.dato_escrito,
  efectores: state.general.efectores,
  localidades: state.busqueda.localidades,
  servicios: state.busqueda.servicios,
});

export default connect(mapStateToProps, mapDispatchToProps)(InputBusqueda);