import { URL_ACTIVIDADES, URL_LOCALIDADES } from '../../constants/Urls';
import { createAction } from 'redux-actions';

export const AGREGAR_BUSQUEDA = createAction('AGREGAR_BUSQUEDA');
export const BLOQUEAR_INTERFACE_BUSQUEDA = createAction('BLOQUEAR_INTERFACE_BUSQUEDA');
export const FILTRAR_EFECTORES = createAction('FILTRAR_EFECTORES');
export const FILTRAR_RESULTADOS = createAction('FILTRAR_RESULTADOS');
export const LIMPIAR_RESULTADOS = createAction('LIMPIAR_RESULTADOS');
export const LIMPIAR_BUSQUEDAS_RECIENTES = createAction('LIMPIAR_BUSQUEDAS_RECIENTES');
export const PUSH_LOCALIDADES = createAction('PUSH_LOCALIDADES');
export const PUSH_SERVICIOS = createAction('PUSH_SERVICIOS');
export const SELECT_DATO_A_BUSCAR = createAction('SELECT_DATO_A_BUSCAR');
export const SORT_EFECTORES = createAction('SORT_EFECTORES');
export const UPDATE_DATO_ESCRITO = createAction('UPDATE_DATO_ESCRITO');
export const UPDATE_SNACK_ORDEN = createAction('UPDATE_SNACK_ORDEN');
export const UPDATE_SNACK_ERROR_BUSQUEDA = createAction('UPDATE_SNACK_ERROR_BUSQUEDA');

export const LIMPIAR_EFECTORES_FILTRADOS = createAction('LIMPIAR_EFECTORES_FILTRADOS');

export const GET_COLECCIONES_BUSQUEDA = () => async (dispatch) => {

  dispatch(BLOQUEAR_INTERFACE_BUSQUEDA(true));



  let localidades = await fetch(URL_LOCALIDADES)
    .then(response => response.json())
    .then(json => {
      dispatch(PUSH_LOCALIDADES(json));
    })
    .catch(err => {
      dispatch(UPDATE_SNACK_ERROR_BUSQUEDA(true));
    });


  let servicios = await fetch(URL_ACTIVIDADES)
    .then(response => response.json())
    .then(json => {
      dispatch(PUSH_SERVICIOS(json));
    })
    .catch(err => {
      dispatch(UPDATE_SNACK_ERROR_BUSQUEDA(true));
    });

  dispatch(BLOQUEAR_INTERFACE_BUSQUEDA(false));
}