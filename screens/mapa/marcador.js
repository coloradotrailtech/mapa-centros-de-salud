import React from 'react';
import { Marker } from 'react-native-maps';

export default function Marcador({ visualizar_detalle, efector }) {
    return (
        <Marker
            coordinate={efector.latlng}
            title={efector.nombre}
            description={efector.direccion}
            onPress={() => visualizar_detalle(efector)}
            icon={
                efector.nivel === "C" ?
                    require("../../assets/images/hospital-3.png") :
                    efector.nivel === "B" ?
                        require("../../assets/images/hospital-2.png")
                        :
                        require("../../assets/images/hospital-1.png")
            }
        />
    );
}