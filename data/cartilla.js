
export const servicios = [
    {     
        cuie: "N95791",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N054416",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03158",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N99980",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05442",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00329",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05438",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05437",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05449",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05455",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03200",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03202",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03201",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00483",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00296",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N10451",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00664",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03159",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N01148",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00419",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N95614",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00020",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03586",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00482",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05448",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N20054",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N95772",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05446",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N20012",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05443",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00205",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00746",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00327",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00018",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00297",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N03584",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00496",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N95809",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05450",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N99979",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N05445",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00420",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00298",
        servicios: [
            {
                nombre: "Salud del adulto",
                profesional: {
                    apellido: "Gimenez",
                    nombres: "Susana"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Laboratorio",
                profesional: {
                    apellido: "Gómez",
                    nombres: "Alexander"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "00:00",
                        fin: "00:00",
                    },
                ],
                
            },
            {
                nombre: "Salud del niño",
                profesional: {
                    apellido: "Bianchi",
                    nombres: "Carlos"
                },
                horarios: [
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Martes, Jueves, Sábado"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Flak",
                    nombres: "Roberta"
                },
                horarios: [
                    {
                        dias: ["Martes", "Miércoles", "Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00204",
        servicios: [
            {
                nombre: "Vacunatorio",
                profesional: {
                    apellido: "Gonzales",
                    nombres: "Juan Alberto"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábados"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Farmacia (entrega de medicamentos)",
                profesional: {
                    apellido: "Arzuid",
                    nombres: "Elizabeth"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Miércoles", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Salud mental",
                profesional: {
                    apellido: "Rodriguez",
                    nombres: "Juan"
                },
                horarios: [
                    {
                        dias: ["Miércoles"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Miércoles"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Guardia de emergencia",
                profesional: {
                    apellido: "Pereira",
                    nombres: "Mercedes"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Lunes", "Viernes"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
        ]
    },
    {
        cuie: "N00328",
        servicios: [
            {
                nombre: "Planificación familiar",
                profesional: {
                    apellido: "Cloney",
                    nombres: "Jorge"
                },
                horarios: [
                    {
                        dias: ["Martes", "Jueves", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00"
                    },
                    {
                        dias: ["Sábado"],
                        inicio: "10:00",
                        fin: "12:00"
                    }
                ],
                
            },
            {
                nombre: "Odontología",
                profesional: {
                    apellido: "Pitt",
                    nombres: "Brad"
                },
                horarios: [
                    {
                        dias: ["Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                ],
                
            },
            {
                nombre: "Enfermería",
                profesional: {
                    apellido: "Messi",
                    nombres: "Lionel"
                },
                horarios: [
                    {
                        dias: ["Jueves"],
                        inicio: "08:00",
                        fin: "12:00",
                    },
                    {
                        dias: ["Jueves"],
                        inicio: "16:00",
                        fin: "20:00",
                    }
                ],
                
            },
            {
                nombre: "Salud de la embarazada",
                profesional: {
                    apellido: "Sosa",
                    nombres: "Juana"
                },
                horarios: [
                    {
                        dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                        inicio: "08:00",
                        fin: "12:00",
                    }
                ],
                
            },
        ]
    },
];