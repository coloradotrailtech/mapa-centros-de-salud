
import { combineReducers } from "redux";

import { persistReducer } from 'redux-persist';
import ExpoFileSystemStorage from "redux-persist-expo-filesystem";

import reducerBusqueda from "./reducerBusqueda";
import reducerGeneral from "./reducerGeneral";

const rootPersistConfig = {
    key: 'root',
    storage: ExpoFileSystemStorage,
    whitelist: ['cartillas', 'efectores', 'busquedas_recientes', 'localidades', 'servicios']
};

const reducerGeneralPersistConfig = {
    key: 'general',
    storage: ExpoFileSystemStorage,
    whitelist: ['cartillas', 'efectores']
};

const reducerBusquedaPersistConfig = {
    key: 'busqueda',
    storage: ExpoFileSystemStorage,
    whitelist: ['busquedas_recientes', 'localidades', 'servicios']
};

let busqueda = persistReducer(reducerBusquedaPersistConfig, reducerBusqueda);
let general = persistReducer(reducerGeneralPersistConfig, reducerGeneral);

const rootReducer = combineReducers({
    busqueda,
    general
});

export default persistReducer(rootPersistConfig, rootReducer);