import { Dimensions } from 'react-native';

export const screen = Dimensions.get('window');
export const ASPECT_RATIO = screen.width / screen.height;
export const LATITUDE_DELTA = 0.0922;
export const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
export const LATITUDE_X_DEFECTO = -27.372053;
export const LONGITUDE_X_DEFECTO = -55.898377;
