import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert, Text, ScrollView } from 'react-native';
import { List } from 'react-native-paper';
import MensajeListado from '../../components/MensajeListado';

class Servicios extends Component {

  formatear_horario(dias) {
    /**
     * Este método formatea los datos de un horario de atención para mostrarlos en un texto.
     */
    let cant_dias = dias.length;
    let texto_dia = dias[0];

    for (let i = 1; i < cant_dias; i++) {
      texto_dia = (i + 1) === cant_dias ? texto_dia + " y " + dias[i] : texto_dia + ", " + dias[i];
    }
    return texto_dia;
  }

  alerta_horario(horario) {
    /**
    * Este método solamente cumple la función lanzar un alert solamente está para ordenar el código.
    */
    let dias = horario.dias;
    let cant_dias = dias.length;
    let texto_dia = cant_dias > 1 ? "Días: " : "Día: ";

    Alert.alert(
      'Horario de atención:',
      "\n" + texto_dia + this.formatear_horario(dias) + ".\n\nHorario: " + horario.inicio + " - " + horario.fin,
      [{ text: 'Cerrar' }]
    );
  }


  render() {
    const { efector } = this.props;
    return (
      <ScrollView >
        {efector.cartillas.length > 0 ?
          <List.Section>
            <List.Subheader>Presione sobre el servicio para ver los horarios</List.Subheader>
            {efector.cartillas.map((cartilla, key) =>
              <List.Section key={key}>               
                  <List.Accordion
                    theme={{ colors: { primary: "#b20000" } }}
                    key={key}
                    title={cartilla.servicio.nombre}
                    left={props => <List.Icon icon="hospital" />}
                  >
                    {cartilla.horarios.map((horario, key) =>
                      <List.Item
                        key={key}
                        onPress={() => this.alerta_horario(horario)}
                        title={this.formatear_horario(horario.dias)}
                        description={horario.inicio + " - " + horario.fin}
                        left={props => <List.Icon icon="clock-outline" />}
                      />
                    )}
                  </List.Accordion>                
              </List.Section>
            )}
          </List.Section>
          :
          <MensajeListado mensaje={"El Centro de Salud aún no definió su cartilla de servicios"} />       
        }
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  efector: state.general.efector
});

export default connect(mapStateToProps)(Servicios);