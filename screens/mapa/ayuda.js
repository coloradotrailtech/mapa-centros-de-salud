import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from 'react-native';
import { Card } from 'react-native-paper';
import Carousel from "react-native-carousel-control";

export default class MapaAyudaScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>

        <Carousel >
          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 310
              }}
                source={require('../../assets/images/ayuda/mapa/1.png')} />
              <Text style={styles.leyenda}>
                Esta app fue hecha para que vos puedas encontrar fácilmente todos los centros de salud que están a tu disposición.
                Presiona sobre alguno de los íconos para conocer sobre el centro de salud y los servicios que brinda.
                </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 346
              }}
                source={require('../../assets/images/ayuda/mapa/2.png')} />
              <Text style={styles.leyenda}>
                Aquí vas a visualizar información agrupada en dos pestañas: "Información general" y "Servicios ofrecidos".
              </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 276
              }} source={require('../../assets/images/ayuda/mapa/3.png')} />
              <Text style={styles.leyenda}>
                En la pestaña de de Información general vas ver una fotografía del centro en cuestión y
                 datos como: nombre, si ofrecen turnos programados, horarios, datos de contacto y dirección.
                 Varios de estos items ofrecen cierta interacción. ¡Presiona sobre ellos para ver que pasa!
              </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 90
              }} source={require('../../assets/images/ayuda/mapa/4.png')} />
              <Text style={styles.leyenda}>Si tú te encuentras dentro o cerca de la provincia podrás ver como llegar al centro de salud. Tienes el servicio de brindado por google y el de Moovit (solo disponible para ciudades donde opera el mismo).</Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 293
              }} source={require('../../assets/images/ayuda/mapa/5.png')} />
              <Text style={styles.leyenda}>En la pestaña de servicios ofrecidos encontrarás un listado con todas los servicios que presta el centro
               de salud y si presionas sobre alguno verás los días y horarios de prestación del mismo.</Text>
            </Card.Content>
          </Card>



        </Carousel>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#666464'
  },
  cabecera: {
    marginTop: 35,
  },
  carta: {
    marginBottom: 8,
  },
  container_header: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagen: {
    width: 256,
    height: 256
  },
  leyenda: {
    marginTop: 10,
    textAlign: 'center',
    color: '#666464'
  },

});
