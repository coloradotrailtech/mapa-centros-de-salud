import React, { Component } from "react";
import { Alert, ScrollView, Image, Linking, StyleSheet, Text, View } from 'react-native';
import { Avatar, Button, Card, Divider, List, Title } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';

export default class InformacionScreen extends Component {

  static navigationOptions = {
    header: null,
    title: 'Información',
  };

  abrir_link(link, msg, titulo) {
    Alert.alert(
      titulo,
      msg,
      [
        { text: 'No', onPress: () => null },
        { text: 'Sí', onPress: () => Linking.openURL(link) },
      ],
    );
  }

  render() {
    const { navigation } = this.props;

    const abrir_ayuda = () => {
      navigation.navigate('InformacionAyuda');
    }

    return (

      <ScrollView>
        <Card style={[styles.carta, styles.cabecera]}>
          <Card.Content>
            <View style={{
              justifyContent: 'flex-end',
              alignItems: 'flex-end'
            }}>
              <Button
                theme={{ colors: { primary: "#424242" } }}
                onPress={() => abrir_ayuda()}
                mode="text"
                style={styles.button}>
                <Ionicons
                  name="md-help-circle-outline"
                  size={20}
                  style={styles.searchIcon}
                />
              </Button>
            </View>
            <View style={styles.container_header}>
              <Image style={styles.imagen} source={require('../../assets/images/icon.png')} />
              <Title>Misalud</Title>
              <Text style={styles.leyenda}>Esta app fue hecha para que vos puedas encontrar fácilmente todos los centros de salud que están a tu disposición</Text>
            </View>
          </Card.Content>
        </Card>

        <Card style={styles.carta}>
          <Card.Content>
            <Title>Páginas web de interés</Title>
            <List.Item
              title="Ministerio de Salud Pública de Misiones"
              description="Sitio web del Ministerio de Salud Pública de Misiones. Aquí encontrarás noticias relacionadas al ámbito y diversos recursos en linea."
              left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/logo-salud.png')} />}
              onPress={() => this.abrir_link('https://salud.misiones.gob.ar/', 'Sitio web del Ministerio de Salud Pública de Misiones. Aquí encontrarás noticias relacionadas al ámbito y diversos recursos en linea. ¿Visitar la web?', 'Ministerio de Salud Pública de Misiones')}
            />
            <Divider />
            <List.Item
              title="Salud con vos"
              description="El portal del programa provincial de salud integral del adolescente del Ministerio de Salud Pública de la Provincia de Misiones que busca empoderar mediante la comunicación a los y las adolescentes."
              left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/salud_con_vos.png')} />}
              onPress={() => this.abrir_link('http://www.saludconvos.misiones.gob.ar/', 'El portal del programa provincial de salud integral del adolescente del Ministerio de Salud Pública de la Provincia de Misiones que busca empoderar mediante la comunicación a los y las adolescentes. ¿Visitar la web?', 'Salud con vos')}
            />
          </Card.Content>
        </Card>

        <Card style={styles.carta}>
          <Card.Content>
            <Title>Teléfonos de interés</Title>
            <List.Item
              title="Emergencias"
              description="911"
              left={() => <List.Icon icon="headset" />}
              onPress={() => this.abrir_link('tel:911', '¿Llamar a emergencias?', 'Atención')}
            />
            <Divider />
            <List.Item
              title="Red de Traslado"
              description="107"
              left={() => <List.Icon icon="ambulance" />}
              onPress={() => this.abrir_link('tel:107', '¿Llamar a la Red de Traslado?', 'Atención')}
            />
          </Card.Content>
        </Card>

        <Card style={styles.carta}>
          <Card.Content>
            <Title>Créditos</Title>           
            <List.Section>
              <List.Accordion
                theme={{ colors: { primary: "#666464" } }}
                title="Íconos"
                left={props => <List.Icon {...props} icon="folder" />}
              >
                <List.Item
                  title="Map"
                  description="Autor: Freepik. Web: www.flaticon.com"
                  left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/creditos/map.png')} />}
                  onPress={() => this.abrir_link('https://www.flaticon.com/authors/freepik', '¿Visitar la web?', 'Atención')} />
                <List.Item
                  title="Hospital"
                  description="Autor: Freepik. Web: www.flaticon.com"
                  left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/creditos/hospital.png')} />}
                  onPress={() => this.abrir_link('https://www.flaticon.com/authors/freepik', '¿Visitar la web?', 'Atención')} />
                <List.Item
                  title="Hospital"
                  description="Autor: mynamepong. Web: www.flaticon.com"
                  left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/creditos/hospital2.png')} />}
                  onPress={() => this.abrir_link('https://www.flaticon.com/authors/mynamepong', '¿Visitar la web?', 'Atención')} />
                <List.Item
                  title="Pharmacy"
                  description="Autor: smalllikeart. Web: www.flaticon.com"
                  left={() => <Avatar.Image style={styles.logo_web} size={55} source={require('../../assets/images/creditos/pharmacy.png')} />}
                  onPress={() => this.abrir_link('https://www.flaticon.com/authors/smalllikeart', '¿Visitar la web?', 'Atención')} />
              </List.Accordion>
            </List.Section>
          </Card.Content>
        </Card>
        <Text style={[styles.leyenda, styles.colorado_trail_tech]}>2020 Colorado Trail Tech</Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cabecera: {
    marginTop: 35,
  },
  carta: {
    marginBottom: 8,
    marginLeft: 10,
    marginRight: 10,
  },
  linea_contenido: {
    padding: 8,
  },
  container_header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagen: {
    width: 128,
    height: 128
  },
  logo_web: {
    top: 10,
  },
  leyenda: {
    textAlign: 'center',
    color: '#666464'
  },
  colorado_trail_tech: {
    padding: 20,
  },
  searchIcon: {

  },
  button: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});