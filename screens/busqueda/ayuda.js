import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from 'react-native';
import { Card } from 'react-native-paper';
import Carousel from "react-native-carousel-control";

export default class BusquedaAyudaScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>

        <Carousel >
          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 186
              }}
                source={require('../../assets/images/ayuda/busquedas/1.png')} />
              <Text style={styles.leyenda}>
                En este apartado visualizarás un historial de búsquedas recientes que hayas realizado. Un botón para poder limpiar este historial
                 y un campo donde podrás realizar búsquedas por nombre de centro de salud, localidad o servicio que preste el centro de salud.
                 Si presionas sobre alguno de los resultados se listarán todos los centros de salud que cumplan con esa característica.
                </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 201
              }}
                source={require('../../assets/images/ayuda/busquedas/2.png')} />
              <Text style={styles.leyenda}>
                Al escribir en el campo la app buscará coincidencias basadas en lo escrito y las listará debajo. SI presionas en el botón: "X" se borrará el contenido del campo de búsqueda.
                Si presionas sobre alguno de los resultados se listarán todos los centros de salud que cumplan con esa característica.
              </Text>
            </Card.Content>
          </Card>

          <Card style={[styles.carta, styles.cabecera, styles.container_header]}>
            <Card.Content style={styles.container_header}>
              <Image style={{
                width: 220,
                height: 356
              }} source={require('../../assets/images/ayuda/busquedas/3.png')} />
              <Text style={styles.leyenda}>
                Puedes ordenar esta lista alfabéticamente o por distancia (si estás dentro o cerca de la provincia).
                Si presionas sobre alguno de los resultados irás al detalle del centro de salud.
              </Text>
            </Card.Content>
          </Card>

        </Carousel>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#666464'
  },
  cabecera: {
    marginTop: 35,
  },
  carta: {
    marginBottom: 8,
  },
  container_header: {
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imagen: {
    width: 256,
    height: 256
  },
  leyenda: {
    marginTop: 10,
    textAlign: 'center',
    color: '#666464'
  },

});
