import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { List, Button } from 'react-native-paper';
import MensajeListado from '../../components/MensajeListado';
import { Ionicons } from '@expo/vector-icons';


export default function BusquedasRecientes({ abrir_ayuda, busquedas_recientes, filtrar_efectores, limpiar }) {

    function Item({ resultado }) {
        return (
            <List.Item
                title={resultado.dato.nombre}
                left={() => <List.Icon icon="history" />}
                onPress={() => filtrar_efectores(resultado)}
            />
        );
    }

    return (
        <View style={styles.container}>
            {busquedas_recientes.length > 0 ?
                <List.Section style={styles.container}>
                    <View style={styles.fixToText}>
                        <List.Subheader>Búsquedas recientes:</List.Subheader>
                        <Button
                            theme={{ colors: { primary: "#424242" } }}
                            onPress={() => limpiar()}
                            mode="text"
                            icon="delete"
                            style={styles.button}>limpiar</Button>
                        <Button
                            theme={{ colors: { primary: "#424242" } }}
                            onPress={() => abrir_ayuda()}
                            mode="text"
                            style={styles.button}>
                            <Ionicons
                                name="md-help-circle-outline"
                                size={20}
                                style={styles.searchIcon}
                            />
                        </Button>
                    </View>
                    <FlatList
                        data={busquedas_recientes}
                        renderItem={({ item }) => <Item resultado={item} />}
                        keyExtractor={item => item.dato.nombre}
                    />
                </List.Section>
                :
                <MensajeListado abrir_ayuda={abrir_ayuda} mensaje={"Aquí aparecerán las búsquedas que vaya realizando"} />
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        color: "#fff",
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 10,
        color: '#f16435',
    }
});