export const localidades = [
    "1º de Mayo",
    "25 de Mayo",
    "9 de Julio Kilómetro 20",
    "9 de Julio Kilómetro 28",
    "Alba Posse",
    "Alicia Alta",
    "Alicia Baja",
    "Almafuerte",
    "Apóstoles",
    "Aristóbulo del Valle",
    "Arroyo del Medio",
    "Azara",
    "Bernardo de Irigoyen",
    "Bonpland",
    "Caá - Yarí",
    "Caburei",
    "Campo Grande",
    "Campo Ramón",
    "Campo Viera",
    "Candelaria",
    "Capioví",
    "Caraguatay",
    "Cerro Azul",
    "Cerro Corá",
    "Colonia Alberdi",
    "Colonia Aurora",
    "Colonia Polana",
    "Colonia Victoria",
    "Colonia Wanda",
    "Comandante Andresito",
    "Concepción de la Sierra",
    "Copioviciño",
    "Corpus",
    "Cruce Caballero",
    "Domingo Savio",
    "Dos Arroyos",
    "Dos de Mayo",
    "Dos Hermanas",
    "El Alcázar",
    "Eldorado",
    "El Salto",
    "El Soberbio",
    "Estación Apóstoles",
    "Florentino Ameghino",
    "Fracrán",
    "Garuhapé",
    "Garupá",
    "General Alvear",
    "General Urquiza",
    "Gobernador López",
    "Gobernador Roca",
    "Guaraní",
    "Helvecia",
    "Hipólito Yrigoyen",
    "Integración",
    "Itacaruaré",
    "Jardín América",
    "Kilómetro 17",
    "La Corita",
    "Laharrague",
    "Leandro N. Alem",
    "Loreto",
    "Los Helechos",
    "María Magdalena",
    "Mártires",
    "Mbopicuá",
    "Mojón Grande",
    "Montecarlo",
    "Nemesio Parma",
    "Nueva Delicia",
    "Oasis",
    "Oberá",
    "Olegario V. Andrade",
    "Panambí",
    "Panambí Kilómetro 15",
    "Panambí Kilómetro 8",
    "Paraíso",
    "Piñalito Norte",
    "Piñalito Sur",
    "Pindapoy",
    "Piray Kilómetro 18",
    "Posadas",
    "Profundidad",
    "Pueblo Illia",
    "Pueblo Nuevo",
    "Puerto Andresito",
    "Puerto Deseado",
    "Puerto Esperanza",
    "Puerto Iguazú",
    "Puerto Leoni",
    "Puerto Libertad",
    "Puerto Mado",
    "Puerto Pinares",
    "Puerto Piray",
    "Puerto Rico",
    "Puerto Santa Ana",
    "Rincón de Azara (Puerto Azara)",
    "Roca Chica",
    "Ruiz de Montoya",
    "Salto Encantado",
    "San Alberto",
    "San Antonio",
    "San Francisco de Asís",
    "San Gotardo",
    "San Ignacio",
    "San Javier",
    "San José",
    "San Martín",
    "San Miguel (Garuhapé-Mi)",
    "San Pedro",
    "Santa Ana",
    "Santa María",
    "Santa Rita",
    "Santiago de Liniers",
    "Santo Pipó",
    "San Vicente",
    "Tarumá",
    "Tobuna",
    "Tres Capones",
    "Valle Hermoso",
    "Villa Akerman",
    "Villa Bonita",
    "Villa Cooperativa",
    "Villa Libertad",
    "Villa Parodi",
    "Villa Roulet",
    "Villa Urrutia"
];

export const unidades_operativas = [
    {
        id: "1",
        nombre: "Salud del adulto"
    },
    {
        id: "2",
        nombre: "Salud de la embarazada"
    },
    {
        id: "3",
        nombre: "Salud del niño"
    },
    {
        id: "4",
        nombre: "Salud del adolescentes"
    },
    {
        id: "5",
        nombre: "Charlas y talleres comunitarios"
    },
    {
        id: "6",
        nombre: "Odontología"
    },
    {
        id: "7",
        nombre: "Enfermería"
    },
    {
        id: "8",
        nombre: "Laboratorio"
    },
    {
        id: "9",
        nombre: "Farmacia (entrega de medicamentos)"
    },
    {
        id: "10",
        nombre: "Vacunatorio"
    },
    {
        id: "11",
        nombre: "Promotores de salud"
    },
    {
        id: "12",
        nombre: "Análisis Clínicos"
    },
    {
        id: "13",
        nombre: "Salud mental"
    },
    {
        id: "14",
        nombre: "Guardia de emergencia"
    },
    {
        id: "15",
        nombre: "Planificación familiar"
    }
];

export const efectores = [
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio Belen - Posadas (Municipal)",
        direccion: "Avenida 405 y los álamos",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        localidad: "Posadas",
        telefono: "0376-4847158",
        latlng: {
            latitude: -27.420611,
            longitude: -55.959252
        },
        cuie: "N95791"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 18 Santa Ines",
        direccion: "Avenida 405 y los álamos",
        localidad: "Garupá",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.52055507,
            longitude: -55.8570972
        },
        cuie: "N05441"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 20 Ñu Pora",
        direccion: "Avenida 405 y los álamos",
        localidad: "Garupá",
        telefono: "0376-4488437",
        latlng: {
            latitude: -27.4475498,
            longitude: -55.860783
        },
        cuie: "N03158"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio Don Pedro (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.4547221,
            longitude: -55.8701777
        },
        cuie: "N99980"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 19 Don Santiago",
        direccion: "Avenida 405 y los álamos",
        localidad: "Garupá",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.46588779,
            longitude: -55.85620772
        },
        cuie: "N05442"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 5 Yacyreta",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4468018",
        latlng: {
            latitude: -27.37477729,
            longitude: -55.92637148
        },
        cuie: "N00329"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 8 Rocamora",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4448798",
        latlng: {
            latitude: -27.376115,
            longitude: -55.9158304
        },
        cuie: "N05438"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 4 La Picada",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4448795",
        latlng: {
            latitude: -27.36898519,
            longitude: -55.91114044
        },
        cuie: "N05437"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Miguel Lanus (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4449025",
        latlng: {
            latitude: -27.4275115,
            longitude: -55.8746666
        },
        cuie: "N05449"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio San Gerardo (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.37505122,
            longitude: -55.93813934
        },
        cuie: "N05455"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Unidad Penal Nro. 4 - Menores Varones",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488416",
        latlng: {
            latitude: -27.42815665,
            longitude: -55.89963913
        },
        cuie: "N03200"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Unidad Penal Nro. 6",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4448440",
        latlng: {
            latitude: -27.4277091,
            longitude: -55.8990169
        },
        cuie: "N03202"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Unidad Penal Nro. 5 - Mujeres",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488417",
        latlng: {
            latitude: -27.4231856,
            longitude: -55.8953154
        },
        cuie: "N03201"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio San Marcos (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.4178858,
            longitude: -55.9210485
        },
        cuie: "N00483"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Los Paraisos (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488571",
        latlng: {
            latitude: -27.4443488,
            longitude: -55.8890331
        },
        cuie: "N00296"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 29 Chacra 32-33",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4458205",
        latlng: {
            latitude: -27.39661991,
            longitude: -55.90668482
        },
        cuie: "N10451"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 25 Barrio Sur Argentino",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4458255",
        latlng: {
            latitude: -27.4285967,
            longitude: -55.9339337
        },
        cuie: "N00664"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 23 San Lorenzo - La Nueva Esperanza Eby A4",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4597416",
        latlng: {
            latitude: -27.4347749,
            longitude: -55.913921
        },
        cuie: "N03159"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 26 Loma Poi - Eby A3-2",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488442",
        latlng: {
            latitude: -27.42182489,
            longitude: -55.88543142
        },
        cuie: "N01148"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "2º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Hospital Dr. Rene Favaloro",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4468050",
        latlng: {
            latitude: -27.3649298,
            longitude: -55.94922604
        },
        cuie: "N00419"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Direccion De Zona De Salud Capital",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4444067",
        latlng: {
            latitude: -27.35728576,
            longitude: -55.90541808
        },
        cuie: "N95614"
    },
    {
        email: "",
        turnos: false,
        nivel: "3º",
        atencion: [
            {
                dias: ["Lunes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Párque De La Salud De La Pcia. De Misiones - Dr.Ramon Madariaga",
        direccion: "Avenida López y planes y Avenida Cabred",
        localidad: "Posadas",
        telefono: "",
        latlng: {
            latitude: -27.38371559,
            longitude: -55.89087694
        },
        cuie: "N00020"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 11 Cecilia Grierson",
        direccion: "Avenida 405 y los álamos",
        localidad: "Garupá",
        telefono: "0376-4444343",
        latlng: {
            latitude: -27.43699628,
            longitude: -55.86961105
        },
        cuie: "N03586"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio Latinoamerica (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4458213",
        latlng: {
            latitude: -27.404585,
            longitude: -55.926322
        },
        cuie: "N00482"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps San Benito",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.4233223,
            longitude: -55.9343923
        },
        cuie: "N05448"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 33 San Isidro",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4597759",
        latlng: {
            latitude: -27.39245913,
            longitude: -55.94471703
        },
        cuie: "N20054"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 30 - Nemesio Parma",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4172778",
        latlng: {
            latitude: -27.4374363,
            longitude: -55.9379483
        },
        cuie: "N95772"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 32 - Itaembe Mini",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.36051403,
            longitude: -55.9992589
        },
        cuie: "N05446"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 21 Independencia",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4595511",
        latlng: {
            latitude: -27.4141651,
            longitude: -55.9561145
        },
        cuie: "N20012"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 17 El Zaiman",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4597444",
        latlng: {
            latitude: -27.4145429,
            longitude: -55.916282
        },
        cuie: "N05443"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 3 Chacra 181 - Esc. Ejercito Argentino",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4458208",
        latlng: {
            latitude: -27.4167097,
            longitude: -55.901624
        },
        cuie: "N00205"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 1 San Miguel",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4468014",
        latlng: {
            latitude: -27.35773506,
            longitude: -55.9170056
        },
        cuie: "N00746"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio Nestor Kirchner",
        direccion: "Caps Nro 12 Villa Flor",
        localidad: "Posadas",
        telefono: "0376-4468013",
        latlng: {
            latitude: -27.36071077,
            longitude: -55.9305278
        },
        cuie: "N00327"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Santa Clara (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4854712",
        latlng: {
            latitude: -27.45501715,
            longitude: -55.93391211
        },
        cuie: "N00018"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 6 Villa Blossett",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4597499",
        latlng: {
            latitude: -27.39768184,
            longitude: -55.89719717
        },
        cuie: "N00297"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Barrio Las Rosas (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Garupá",
        telefono: "0376-4580196",
        latlng: {
            latitude: -27.45238372,
            longitude: -55.84555266
        },
        cuie: "N03584"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Dos De Abril (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4444072",
        latlng: {
            latitude: -27.36708175,
            longitude: -55.88611948
        },
        cuie: "N00496"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Colonia Aeroparque (Municipal)",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4597855",
        latlng: {
            latitude: -27.39802476,
            longitude: -55.92284797
        },
        cuie: "N95809"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 28 Barrio Tacuru",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4459300",
        latlng: {
            latitude: -27.39727387,
            longitude: -55.93349643
        },
        cuie: "N05450"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 16 Santa Rita",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4491403",
        latlng: {
            latitude: -27.370735,
            longitude: -56.003153
        },
        cuie: "N99979"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 13 Villa Lanus",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4468046",
        latlng: {
            latitude: -27.38821399,
            longitude: -55.9216392
        },
        cuie: "N05445"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 7 Sesquicentenario",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4468012",
        latlng: {
            latitude: -27.38456034,
            longitude: -55.93837547
        },
        cuie: "N00420"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 15 Alta Gracia",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488438",
        latlng: {
            latitude: -27.432485,
            longitude: -55.8845817
        },
        cuie: "N00298"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps Nro 10 Garupá",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4488441",
        latlng: {
            latitude: -27.42337,
            longitude: -55.8988387
        },
        cuie: "N00204"
    },
    {
        email: "efector@gmail.com",
        turnos: true,
        nivel: "1º",
        atencion: [
            {
                dias: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                inicio: "08:00",
                fin: "22:00",
            }
        ],
        nombre: "Caps San Isidro - Barrio Oleros",
        direccion: "Avenida 405 y los álamos",
        localidad: "Posadas",
        telefono: "0376-4458207",
        latlng: {
            latitude: -27.3954663,
            longitude: -55.9187697
        },
        cuie: "N00328"
    },
];
