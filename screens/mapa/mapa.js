import React from 'react';
import MapView from 'react-native-maps';
import Marcador from './marcador';
import { StyleSheet } from 'react-native';

export default function Mapa({ coordenadas_en_rango, efectores, ubicacionUsuario, visualizar_detalle }) {
    return (
        <MapView
            style={styles.map}
            ref={ref => { this.map = ref; }}
            initialRegion={ubicacionUsuario}
            showsUserLocation={coordenadas_en_rango}
            showsMyLocationButton={false}
            showsCompass={false}
            showsBuildings={false}
            showsTraffic={false}
            showsIndoors={false}
            zoomControlEnabled={false}
            toolbarEnabled={false}
            loadingEnabled={true}
        >
            {
                efectores.map(efector =>
                    (
                        <Marcador visualizar_detalle={visualizar_detalle} key={efector.id} efector={efector} />
                    ))
            }
        </MapView>
    );
}

const styles = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});