import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Alert,
  Animated,
  ScrollView,
  StyleSheet,
  View,
  Linking
} from 'react-native';
import { List, Button, Colors } from 'react-native-paper';
import { Persmissions, Notifications } from 'expo';
import { HEADER_SCROLL_DISTANCE, HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT } from '../../constants/LayoutHeader';
import MensajeListado from '../../components/MensajeListado';

class Informacion extends Component {

  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
    };
  }

  /* Te comenté esto por ahora porque me saltaba una excepción
  componentDidMount() {
    this.pushNotif();
  }

  pushNotif = async () => {
    let token = await Notifications.askAsync(Persmissions.NOTIFICATIONS);
  }
  */

  formatear_horario(dias) {
    /**
     * Este método formatea los datos de un horario de atención para mostrarlos en un texto.
     */
    let cant_dias = dias.length;
    let texto_dia = dias[0];

    for (let i = 1; i < cant_dias; i++) {
      texto_dia = (i + 1) === cant_dias ? texto_dia + " y " + dias[i] : texto_dia + ", " + dias[i];
    }
    return texto_dia;
  }

  alerta_horario(horario) {
    /**
    * Este método solamente cumple la función lanzar un alert solamente está para ordenar el código.
    */
    let dias = horario.dias;
    let cant_dias = dias.length;
    let texto_dia = cant_dias > 1 ? "Días: " : "Día: ";

    Alert.alert(
      'Horario de atención:',
      "\n" + texto_dia + this.formatear_horario(dias) + ".\n\nHorario: " + horario.inicio + " - " + horario.fin,
      [{ text: 'Cerrar' }]
    );
  }

  abrir_link(link, msg) {
    Alert.alert(
      'Atención:',
      msg,
      [
        { text: 'No', onPress: () => null },
        { text: 'Sí', onPress: () => Linking.openURL(link) },
      ],
    );
  }

  alerta_informacion(msg) {
    Alert.alert(
      'Información:',
      msg,
      [
        { text: 'Cerrar', onPress: () => null },
      ],
    );
  }

  abrir_ruta(ubicacionUsuario, latlng) {
    Linking.openURL('https://www.google.com/maps/dir/' + ubicacionUsuario.latitude + ', ' + ubicacionUsuario.longitude + '/' + latlng.latitude + ',' + latlng.longitude);
  }

  abrir_moovit(efector) {
    dest_lat = efector.latlng.latitude;
    dest_lon = efector.latlng.longitude;
    dest_name = efector.nombre.replace(' ', '%20');
    Linking.openURL('moovit://directions?dest_lat=' + dest_lat + '&dest_lon=' + dest_lon + '&dest_name=' + dest_name + '&auto_run=true&partner_id=mapas-redux').catch(function (error) {
      Linking.openURL('https://moovit.com/?to=' + dest_name + '&tll=' + dest_lat + '_' + dest_lon + '&metroId=' + efector.localidad.nombre + '&lang=es');
    });
  }

  aproximar_distancia(origen, destino) {
    rad = function (x) { return x * Math.PI / 180; }
    let radio = 6378.137; //Radio de la tierra en km
    let dLat = rad(destino.latitude - origen.latitude);
    let dLong = rad(destino.longitude - origen.longitude);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(origen.latitude)) * Math.cos(rad(destino.latitude)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = radio * c;

    if (d > 0) {
      return d.toFixed(1).toString().replace(".", ",") + ' km';
    } else {
      let x = d.toFixed(3).toString().split('.');
      return x[1] + 'm'
    }
  }

  renderScrollViewContent() {
    const { coordenadas_en_rango, efector, ubicacionUsuario } = this.props;
    return (
      <View style={styles.scrollViewContent}>
        <List.Section>
          <List.Item
            title="Centro de Salud:"
            description={efector.nombre}
            left={() => <List.Icon icon="hospital-building" />}
          />
          {
            efector.turnos ?
              <List.Item
                theme={{ colors: { text: Colors.green500 } }}
                title="Ofrece turnos programados"
                description="Este centro de salud ofrece turnos a la población que lo solicite"
                left={() => <List.Icon color={Colors.green500} icon="calendar-check" />}
              />
              :
              <List.Item
                theme={{ colors: { text: Colors.red500 } }}
                title="No ofrece turnos programados"
                description="Este centro de salud no ofrece el servicio turnos programados a la población"
                left={() => <List.Icon color={Colors.red500} icon="calendar-remove" />}
              />
          }
          <List.Subheader>Horarios de atención:</List.Subheader>

          {efector.atencion.length > 0  ?
            efector.atencion.map((horario, key) =>
              <List.Item
                key={key}
                onPress={() => this.alerta_horario(horario)}
                title={this.formatear_horario(horario.dias)}
                description={horario.inicio + " - " + horario.fin}
                left={props => <List.Icon icon="clock-outline" />}
              />
            )
            :
            <List.Item
              title="Aún no definido"
              onPress={() => this.alerta_informacion('El Centro de Salud aún no cargó sus horarios de atención. Sepa disculpar las molestias y también que este dato prontamente será actualizado.')}
              description="Este dato pronto se actualizará"
              left={props => <List.Icon icon="clock-outline" />}
            />
          }


          <List.Subheader>Contacto y dirección:</List.Subheader>
          {
            efector.telefono ?
              <List.Item
                title="Teléfono/s:"
                description={efector.telefono}
                left={() => <List.Icon icon="phone-in-talk" />}
                onPress={() => this.abrir_link('tel: ' + efector.telefono, '¿Desea comunicarse telefónicamente con el centro de salud?')}
              />
              :
              null
          }
          {
            efector.email ?
              <List.Item
                title="Email:"
                description={efector.email}
                left={() => <List.Icon icon="email" />}
                onPress={() => this.abrir_link('mailto: ' + efector.email, '¿Desea contactarse a través de correo electrónico con el centro de salud?')}
              />
              :
              null
          }
          <List.Item
            title="Localidad:"
            description={efector.localidad.nombre}
            left={() => <List.Icon icon="city-variant-outline" />}
          />
          {coordenadas_en_rango ?
            <List.Item
              title="Dirección:"
              description={efector.direccion + "\nA " + this.aproximar_distancia(ubicacionUsuario, efector.latlng) + " de usted"}
              left={() => <List.Icon icon="directions" />}
              onPress={() => this.alerta_informacion('Tenga en cuenta que esta aproximación resulta de la hipotética distancia en linea recta existente entre su ubicación y la del centro de salud seleccionado.')}
            />
            :
            <List.Item
              title="Dirección:"
              description={efector.direccion}
              left={() => <List.Icon icon="directions" />}
            />
          }
        </List.Section>
        {
          coordenadas_en_rango ?
            <View>
              <Button icon="near-me"
                mode="contained"
                style={[styles.button, styles.colorButton]}
                onPress={() => this.abrir_ruta(ubicacionUsuario, efector.latlng)}>
                Mostrar como llegar
            </Button>          
            <Button icon={require("../../assets/images/moovit.png")}
                mode="contained"
                style={[styles.button, styles.buttonMoovit]}
                onPress={() => this.abrir_moovit(efector)}>
                Mostrar en Moovit
            </Button>               
            </View>
            :
            null
        }

      </View>
    );
  };

  render() {

    const { efector } = this.props;

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 2, 0],
      extrapolate: 'clamp',
    });

    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.fill}>
        <ScrollView
          style={styles.fill}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
          )}
        >
          {this.renderScrollViewContent()}
        </ScrollView>

        <Animated.View style={[styles.header, { height: headerHeight }]}>
          <Animated.Image
            style={[
              styles.backgroundImage,
              { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },
            ]}
            source={{ uri: efector.image }}
          />
        </Animated.View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  fill: {
    flex: 1,
  },
  row: {
    height: 40,
    marginTop: 10,
    backgroundColor: '#fbfbfb',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#D3D3D3',
    overflow: 'hidden',
  },
  bar: {
    marginTop: 28,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 18,
  },
  scrollViewContent: {
    marginTop: HEADER_MAX_HEIGHT,
  },
  button: {
    marginBottom: 3,
    marginLeft: 10,
    marginRight: 10,

  },
  colorButton: {
    backgroundColor: '#4688f4',
  },
  buttonMoovit: {
    backgroundColor: '#f16435',
    marginBottom: 10,
  }
});

const mapStateToProps = (state) => ({
  coordenadas_en_rango: state.general.coordenadas_en_rango,
  efector: state.general.efector,
  ubicacionUsuario: state.general.ubicacionUsuario
});

export default connect(mapStateToProps, {})(Informacion);
