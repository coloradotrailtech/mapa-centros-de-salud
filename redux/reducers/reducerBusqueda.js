import { handleActions } from 'redux-actions';

import {
    AGREGAR_BUSQUEDA,
    BLOQUEAR_INTERFACE_BUSQUEDA,
    FILTRAR_EFECTORES,
    FILTRAR_RESULTADOS,
    LIMPIAR_RESULTADOS,
    LIMPIAR_BUSQUEDAS_RECIENTES,
    PUSH_LOCALIDADES,
    PUSH_SERVICIOS,
    SELECT_DATO_A_BUSCAR,
    SORT_EFECTORES,
    UPDATE_DATO_ESCRITO,
    UPDATE_SNACK_ORDEN,
    UPDATE_SNACK_ERROR_BUSQUEDA,
    LIMPIAR_EFECTORES_FILTRADOS
} from '../actions/busqueda';

const initialState = {
    bloquear_interface_busqueda: false,
    busquedas_recientes: [],
    dato_a_buscar: {},// este es un objeto que es sugerido por nosotros y escogido por el usuario y que lo armamos en FILTRAR_RESULTADOS a partir del dato que escribe el mismo.
    dato_escrito: "", // este es lo que escribe el usuario en el imput de búsqueda
    efectores_filtrados: [],
    mostrar_snack_orden: false,
    mostrar_snack_error_busqueda: false,
    localidades: [],
    resultados_filtrados: [],
    servicios: [],
    refreshing: false,
    tipo_ordenamiento: "nombre" //para ordenar los efectores filtrados. Puede ser por nombre o distancia
}

export default reducerBusqueda = handleActions({
    [AGREGAR_BUSQUEDA]: (state, action) => {
        return {
            ...state,
            busquedas_recientes: state.busquedas_recientes.concat(action.payload),
        };
    },
    [BLOQUEAR_INTERFACE_BUSQUEDA]: (state, action) => {
        return {
            ...state,
            bloquear_interface_busqueda: action.payload
        };
    },
    [FILTRAR_EFECTORES]: (state, action) => {

        let { dato_a_buscar, efectores, cartillas, ubicacionUsuario, coordenadas_en_rango } = action.payload;
        let resultados = [];

        /**
         * Filtramos centros de salud por la característica solicitada y ordenamos la lista.
         */
        switch (dato_a_buscar.tipo) {
            case "localidades":
                resultados = efectores.filter(efector => efector.localidad.nombre == dato_a_buscar.dato.id);
                break;
            case "efectores":
                resultados = efectores.filter(efector => efector.cuie == dato_a_buscar.dato.id);
                break;
            case "servicios":

                let cuies = cartillas.filter(cartilla => cartilla.servicio.id == dato_a_buscar.dato.id).reduce((ids, efector) => {
                    return [...ids, efector.cuie]
                }, []);

                resultados = efectores.filter(efector => cuies.includes(efector.cuie));
                break;
        }

        let efectores_medidos = resultados;

        if (coordenadas_en_rango) { //si está en rango agregamos el dato de la distancia.
            efectores_medidos = resultados.reduce((medidos, efector) => {
                rad = function (x) { return x * Math.PI / 180; }
                let radio = 6378.137; //Radio de la tierra en km
                let dLat = rad(efector.latlng.latitude - ubicacionUsuario.latitude);
                let dLong = rad(efector.latlng.longitude - ubicacionUsuario.longitude);
                let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(ubicacionUsuario.latitude)) * Math.cos(rad(efector.latlng.latitude)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
                let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                let d = radio * c;
                let distancia = d.toFixed(1).toString().replace(".", ",");
                return [...medidos, { ...efector, distancia: distancia }]
            }, []);
        }


        let resultados_ordenado = efectores_medidos.sort(function (a, b) {
            return a[state.tipo_ordenamiento] < b[state.tipo_ordenamiento] ? -1 : a[state.tipo_ordenamiento] > b[state.tipo_ordenamiento] ? 1 : 0;
        });

        /**
         * Agregamos el nuevo elemento al historial de búsqueda y ordenamos la lista.
         */
        let busquedas_recientes = state.busquedas_recientes.concat(action.payload.dato_a_buscar);
        let busquedas_recientes_sin_repetidos = busquedas_recientes.filter((valorActual, indiceActual, arreglo) => {
            return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
        });


        return {
            ...state,
            busquedas_recientes: busquedas_recientes_sin_repetidos,
            efectores_filtrados: resultados_ordenado,
            bloquear_interface_busqueda: false,
            refreshing: false,
            mostrar_snack_orden: true
        };
    },
    [FILTRAR_RESULTADOS]: (state, action) => {

        let { dato, localidades, servicios, efectores } = action.payload;
        let resultados = [];

        /**
         * Filtramos los set de datos de servicios, centros de salud y localidades con los que incluyan en su nombre
         * el dato buscado.
         */

        let localidades_filtradas = localidades.filter(localidad => localidad.nombre.toLowerCase().includes(dato.toLowerCase()));
        let servicios_filtrados = servicios.filter(servicio => servicio.nombre.toLowerCase().includes(dato.toLowerCase()));
        let efectores_filtrados = efectores.filter(efector => efector.nombre.toLowerCase().includes(dato.toLowerCase()));

        /**
         * Armamos una lista con los registros filtrados indicando de que tipo se trata y ordenamos esa lista.
         */

        localidades_filtradas.forEach(function (elemento) {
            let { id, nombre } = elemento;
            resultados.push({ dato: { nombre, id: nombre }, tipo: "localidades" });
        });

        servicios_filtrados.forEach(function (elemento) {
            let { id, nombre } = elemento;
            resultados.push({ dato: { nombre, id }, tipo: "servicios" });
        });

        efectores_filtrados.forEach(function (elemento) {
            let { cuie, nombre } = elemento;
            resultados.push({ dato: { nombre: nombre, id: cuie }, tipo: "efectores" });
        });

        let resultados_ordenado = resultados.sort(function (a, b) {
            return a.dato.nombre < b.dato.nombre ? -1 : a.dato.nombre > b.dato.nombre ? 1 : 0;
        });

        return {
            ...state,
            dato_escrito: dato,
            resultados_filtrados: resultados_ordenado,
        };
    },

    [LIMPIAR_RESULTADOS]: (state, action) => {
        return {
            ...state,
            dato_escrito: "",
            resultados_filtrados: [],
            efectores_filtrados: []
        };
    },
    [LIMPIAR_EFECTORES_FILTRADOS]: (state, action) => {
        let efectores_filtrados_resguardo = state.efectores_filtrados;
        return {
            ...state,
            efectores_filtrados_resguardo: efectores_filtrados_resguardo,
            efectores_filtrados: []
        };
    },
    [LIMPIAR_BUSQUEDAS_RECIENTES]: (state, action) => {
        return {
            ...state,
            busquedas_recientes: []
        };
    },
    [PUSH_LOCALIDADES]: (state, action) => {
        return {
            ...state,
            localidades: action.payload
        };
    },
    [PUSH_SERVICIOS]: (state, action) => {
        return {
            ...state,
            servicios: action.payload
        };
    },
    [SELECT_DATO_A_BUSCAR]: (state, action) => {
        return {
            ...state,
            dato_a_buscar: action.payload,
        };
    },
    [SORT_EFECTORES]: (state, action) => {
        return {
            ...state,
            refreshing: true,
            tipo_ordenamiento: action.payload,
        };
    },
    [UPDATE_DATO_ESCRITO]: (state, action) => {
        return {
            ...state,
            dato_escrito: action.payload,
        };
    },
    [UPDATE_SNACK_ERROR_BUSQUEDA]: (state, action) => {
        return {
            ...state,
            mostrar_snack_error_busqueda: action.payload
        };
    },
    [UPDATE_SNACK_ORDEN]: (state, action) => {
        return {
            ...state,
            mostrar_snack_orden: action.payload
        };
    },
}, initialState);
