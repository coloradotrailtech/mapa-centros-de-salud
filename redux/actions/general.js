import { URL_EFECTORES, URL_CARTILLAS } from '../../constants/Urls';
import { createAction } from 'redux-actions';

export const BLOQUEAR_INTERFACE_MAPA = createAction('BLOQUEAR_INTERFACE_MAPA');
export const PUSH_EFECTORES = createAction('PUSH_EFECTORES');
export const PUSH_CARTILLAS = createAction('PUSH_CARTILLAS');
export const SELECT_EFECTOR = createAction('SELECT_EFECTOR');
export const UPDATE_UBICACION_USUARIO = createAction('UPDATE_UBICACION_USUARIO');
export const UPDATE_SNACK_COORDENADAS = createAction('UPDATE_SNACK_COORDENADAS');
export const UPDATE_SNACK_ERROR_GENERAL = createAction('UPDATE_SNACK_ERROR_GENERAL');

export const ENCONTRAR_COORDENADAS = () => async (dispatch) => {
    navigator.geolocation.getCurrentPosition(
        posicion => {
            /**
             * Acá hacemos un pequeño control para determinar si el usuario se encuentra medianamente cerca de la provincia
             * verificando si la posición se encuentra en un cuadrante que cubre la provincia y un poco de las 
             */
            let latitud_valida = (-28.392059 <= posicion.coords.latitude) && (-25.338133 >= posicion.coords.latitude) ? true : false;
            let logitud_valida = (-56.153103 <= posicion.coords.longitude) && (-53.581345 >= posicion.coords.longitude) ? true : false;
            let coordenadas_en_rango = latitud_valida && logitud_valida ? true : false;
            let coordenadas = posicion.coords;
            dispatch(UPDATE_UBICACION_USUARIO({ coordenadas_en_rango, coordenadas }));
        },
        error => Alert.alert(error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
}

export const GET_COLECCIONES_MAPA = () => async (dispatch) => {

    dispatch(BLOQUEAR_INTERFACE_MAPA(true));

    let efectores = await fetch(URL_EFECTORES)
        .then(response => response.json())
        .then(json => {
            dispatch(PUSH_EFECTORES(json));
        })
        .catch(err => {
            console.log("error con efectores");
            dispatch(UPDATE_SNACK_ERROR_GENERAL(true));
        });

    let cartillas = await fetch(URL_CARTILLAS)
        .then(response => response.json())
        .then(json => {
            console.log("todo bien con cartillas");
            dispatch(PUSH_CARTILLAS(json));
        })
        .catch(err => {
            console.log("error con cartilla");
            dispatch(UPDATE_SNACK_ERROR_GENERAL(true));
        });

    dispatch(BLOQUEAR_INTERFACE_MAPA(false));
}