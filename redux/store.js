import { createStore, applyMiddleware, compose } from "redux";
import { persistStore } from 'redux-persist';
import thunk from "redux-thunk";
import reducerPrincipal from "./reducers/index";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    let store = createStore(reducerPrincipal, composeEnhancers(applyMiddleware(thunk)));
    let persistor = persistStore(store)
    return { store, persistor }
}