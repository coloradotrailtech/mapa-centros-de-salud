import {createAppContainer} from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import Informacion from '../../screens/Efector/informacion';
import Servicios from '../../screens/Efector/servicios';
 
const TabScreen = createMaterialTopTabNavigator(
  {
    'Información general': { screen: Informacion },
    'Servicios ofrecidos': { screen: Servicios },
  },
  {
    tabBarPosition: 'top',
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: '#2b2b2b',
      style: {
        backgroundColor: '#ffb50e',
      },
      labelStyle: {
        textAlign: 'center',
      },
      indicatorStyle: {
        borderBottomColor: '#b20000',
        borderBottomWidth: 3,
      },
    },
  }
);

const TabsScreen = createStackNavigator({
  TabScreen: {
    screen: TabScreen,
    navigationOptions: {
        header: null,
    },
  },
});

export default createAppContainer(TabsScreen);