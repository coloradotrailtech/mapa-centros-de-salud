import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import TabBarIcon from '../../components/TabBarIcon';
import MapaScreen from '../../screens/mapa';
import MapaAyudaScreen from '../../screens/mapa/ayuda';
import EfectorScreen from '../efector/DetalleEfectorNavigator';
import InformacionScreen from '../../screens/informacion';
import InformacionAyudaScreen from '../../screens/informacion/ayuda';
import BusquedaScreen from '../../screens/busqueda';
import BusquedaAyudaScreen from '../../screens/busqueda/ayuda';
import EfectoresFiltrados from '../../screens/busqueda/efectoresFiltrados';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const MapaStack = createStackNavigator(
  {
    Mapa: MapaScreen,
    Detalle: EfectorScreen,
    MapaAyuda: MapaAyudaScreen
  },
  config
);

MapaStack.navigationOptions = {
  tabBarLabel: 'Mapa',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-map'
          : 'md-map'
      }
    />
  ),
};

MapaStack.path = '';

const BusquedaStack = createStackNavigator(
  {
    Busqueda: BusquedaScreen,
    EfectoresFiltrados: EfectoresFiltrados,
    BusquedaAyuda: BusquedaAyudaScreen
  },
  config
);

BusquedaStack.navigationOptions = {
  tabBarLabel: 'Búsqueda',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
  ),
};

BusquedaStack.path = '';

const InformacionStack = createStackNavigator(
  {
    Informacion: InformacionScreen,
    InformacionAyuda: InformacionAyudaScreen,
  },
  config
);

InformacionStack.navigationOptions = {
  tabBarLabel: 'Información',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'md-information-circle-outline' : 'md-information-circle-outline'} />
  ),
};

InformacionStack.path = '';

const tabNavigator = createMaterialBottomTabNavigator({
  MapaStack,
  BusquedaStack,
  InformacionStack
}, {
  initialRouteName: 'MapaStack',
  activeColor: '#666464',
  inactiveColor: '#828185',
  barStyle: { backgroundColor: 'white' },
});

tabNavigator.path = '';

export default tabNavigator;