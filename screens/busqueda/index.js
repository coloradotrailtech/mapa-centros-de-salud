import React, { Component } from "react";
import NetInfo from '@react-native-community/netinfo';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  GET_COLECCIONES_BUSQUEDA,
  LIMPIAR_BUSQUEDAS_RECIENTES,
  SELECT_DATO_A_BUSCAR,
  UPDATE_SNACK_ERROR_BUSQUEDA
} from '../../redux/actions/busqueda';
import { Alert, StyleSheet, View } from 'react-native';
import { Snackbar } from 'react-native-paper';
import BusquedasRecientes from './busquedasRecientes';
import IndicadorCarga from '../../components/IndicadorCarga';
import InputBusqueda from './inputBusqueda';
import Resultados from './resultados';


class BusquedaScreen extends Component {

  static navigationOptions = {
    headerTitle: () => <InputBusqueda />,
    headerStyle: {
      backgroundColor: '#fff',
    },
  };

  componentDidMount() {
    const { GET_COLECCIONES_BUSQUEDA } = this.props;

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        GET_COLECCIONES_BUSQUEDA();
      }
    });
  }

  render() {

    const {
      bloquear_interface_busqueda,
      busquedas_recientes,
      dato_escrito,
      mostrar_snack_error_busqueda,
      navigation,
      resultados_filtrados,
      LIMPIAR_BUSQUEDAS_RECIENTES,
      UPDATE_SNACK_ERROR_BUSQUEDA,
      SELECT_DATO_A_BUSCAR } = this.props;

    const filtrar_efectores = (dato) => {
      SELECT_DATO_A_BUSCAR(dato);
      navigation.navigate('EfectoresFiltrados');
    }

    const limpiar_hitorial_busquedas_recientes = () => {
      Alert.alert(
        'Atención:',
        '¿Limpiar el historial de búsquedas recientes?',
        [
          { text: 'No', onPress: () => null },
          { text: 'Sí', onPress: () => LIMPIAR_BUSQUEDAS_RECIENTES() },
        ],
      );
    }

    const abrir_ayuda = () => {
      navigation.navigate('BusquedaAyuda');
    }

    return (
      <View style={styles.container} >
        {!bloquear_interface_busqueda ?
          dato_escrito !== "" ?
            <Resultados
              abrir_ayuda={abrir_ayuda}
              resultados_filtrados={resultados_filtrados}
              filtrar_efectores={filtrar_efectores} />
            :
            <BusquedasRecientes
              limpiar={limpiar_hitorial_busquedas_recientes}
              abrir_ayuda={abrir_ayuda}
              busquedas_recientes={busquedas_recientes}
              filtrar_efectores={filtrar_efectores}
            />
          :
          <IndicadorCarga />
        }
        <Snackbar
          visible={mostrar_snack_error_busqueda}
          onDismiss={() => UPDATE_SNACK_ERROR_BUSQUEDA(false)}
          action={{
            label: 'Cerrar',
            onPress: () => {
              UPDATE_SNACK_ERROR_BUSQUEDA(false);
            },
          }}
        > 
          Ocurre un problema para actualizar los datos de búsqeda.
        </Snackbar>
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});


const mapStateToProps = (state) => ({
  resultados_filtrados: state.busqueda.resultados_filtrados,
  busquedas_recientes: state.busqueda.busquedas_recientes,
  dato_escrito: state.busqueda.dato_escrito,
  mostrar_snack_error_busqueda: state.busqueda.mostrar_snack_error_busqueda,
  bloquear_interface_busqueda: state.busqueda.bloquear_interface_busqueda,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  { GET_COLECCIONES_BUSQUEDA, LIMPIAR_BUSQUEDAS_RECIENTES, SELECT_DATO_A_BUSCAR, UPDATE_SNACK_ERROR_BUSQUEDA }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BusquedaScreen);
