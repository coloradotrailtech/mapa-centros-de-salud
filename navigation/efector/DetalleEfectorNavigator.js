import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import TabsScreen from './DetalleEfectorTabNavigator';
import IndicadorCarga from '../../components/IndicadorCarga';


class EfectorScreen extends Component {

  static navigationOptions = {
    headerTitle: "Detalle del Centro de Salud",
    headerStyle: {
      backgroundColor: '#fff',
    },
  };

  render() {

    const { efectores_filtrados } = this.props;

    return (
      <View style={styles.container}>
        {efectores_filtrados ?
          <IndicadorCarga />
          :
          <TabsScreen />
        }
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

const mapStateToProps = (state) => ({
  efectores_filtrados: state.general.efectores_filtrados,
});

export default connect(mapStateToProps)(EfectorScreen);