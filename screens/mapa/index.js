import React, { Component } from "react";
import NetInfo from '@react-native-community/netinfo';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    BLOQUEAR_INTERFACE_MAPA,
    ENCONTRAR_COORDENADAS,
    GET_COLECCIONES_MAPA,
    SELECT_EFECTOR,
    UPDATE_SNACK_COORDENADAS,
    UPDATE_SNACK_ERROR_GENERAL
} from '../../redux/actions/general';
import { StyleSheet, View } from 'react-native';
import { Snackbar, Button } from 'react-native-paper';
import IndicadorCarga from '../../components/IndicadorCarga';
import Mapa from './mapa';

class MapaScreen extends Component {

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        const { GET_COLECCIONES_MAPA, ENCONTRAR_COORDENADAS } = this.props;
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                GET_COLECCIONES_MAPA();
            }
        });
        ENCONTRAR_COORDENADAS();
    }

    render() {

        const {
            bloquear_interface_mapa,
            coordenadas_en_rango,
            efectores,
            mostrar_snack_coordenadas,
            mostrar_snack_error_general,
            navigation,
            ubicacionUsuario,
            SELECT_EFECTOR,
            UPDATE_SNACK_COORDENADAS,
            UPDATE_SNACK_ERROR_GENERAL } = this.props;

        const select_efector = (efector) => {
            BLOQUEAR_INTERFACE_MAPA(true);
            SELECT_EFECTOR(efector);
            navigation.navigate('Detalle');
        }

        const abrir_ayuda = () => {
            navigation.navigate('MapaAyuda');
        }

        return (
            <View style={styles.container}>
                {ubicacionUsuario !== "" && !bloquear_interface_mapa ?
                    <View style={styles.container}>

                        <Mapa coordenadas_en_rango={coordenadas_en_rango}
                            visualizar_detalle={select_efector}
                            efectores={efectores}
                            ubicacionUsuario={ubicacionUsuario} />
                            
                        <Button
                            theme={{ colors: { primary: "#424242" } }}
                            icon="help-circle-outline"
                            onPress={() => abrir_ayuda()}
                            mode="text"
                            style={styles.button}>
                            ayuda
                        </Button>
                        <Snackbar
                            visible={mostrar_snack_coordenadas}
                            onDismiss={() => UPDATE_SNACK_COORDENADAS(false)}
                            action={{
                                label: 'Cerrar',
                                onPress: () => {
                                    UPDATE_SNACK_COORDENADAS(false);
                                },
                            }}
                        >
                            Usted se encuentra fuera del área de cobertura prevista por la app.
                        </Snackbar>
                        <Snackbar
                            visible={mostrar_snack_error_general}
                            onDismiss={() => UPDATE_SNACK_ERROR_GENERAL(false)}
                            action={{
                                label: 'Cerrar',
                                onPress: () => {
                                    UPDATE_SNACK_ERROR_GENERAL(false);
                                },
                            }}
                        >
                            Ocurre un problema para actualizar los datos de los centros de salud.
                        </Snackbar>
                    </View>
                    :
                    <IndicadorCarga />
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    button: {
        alignContent: 'flex-end'
    },
    searchIcon: {
        padding: 3,
        marginRight: 5,
    },
});

const mapStateToProps = (state) => ({
    bloquear_interface_mapa: state.general.bloquear_interface_mapa,
    coordenadas_en_rango: state.general.coordenadas_en_rango,
    efectores: state.general.efectores,
    mostrar_snack_coordenadas: state.general.mostrar_snack_coordenadas,
    mostrar_snack_error_general: state.general.mostrar_snack_error_general,
    ubicacionUsuario: state.general.ubicacionUsuario,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    BLOQUEAR_INTERFACE_MAPA,
    GET_COLECCIONES_MAPA,
    ENCONTRAR_COORDENADAS,
    SELECT_EFECTOR,
    UPDATE_SNACK_COORDENADAS,
    UPDATE_SNACK_ERROR_GENERAL
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MapaScreen);
